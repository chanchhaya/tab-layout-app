package com.chhaya.fragment6_8;

import androidx.appcompat.app.AppCompatActivity;

import android.content.res.Configuration;
import android.os.Bundle;

import com.chhaya.fragment6_8.fragments.GeneralSettingFragment;
import com.chhaya.fragment6_8.fragments.SettingFragment;
import com.chhaya.fragment6_8.helper.FragmentHelper;

public class SettingActivity extends AppCompatActivity {

    private SettingFragment settingFragment;
    private GeneralSettingFragment generalSettingFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        settingFragment = new SettingFragment();
        generalSettingFragment = new GeneralSettingFragment();

        if (Configuration.ORIENTATION_PORTRAIT == getResources().getConfiguration().orientation) {
            FragmentHelper.add(this,
                    R.id.f_setting_container,
                    settingFragment);
        } else if (Configuration.ORIENTATION_LANDSCAPE == getResources().getConfiguration().orientation) {
            FragmentHelper.add(this, R.id.fl_container, settingFragment);
            FragmentHelper.add(this, R.id.fr_container, generalSettingFragment);
        }

    }
}
