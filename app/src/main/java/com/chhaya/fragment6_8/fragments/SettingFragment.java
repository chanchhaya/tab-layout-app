package com.chhaya.fragment6_8.fragments;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.chhaya.fragment6_8.R;
import com.chhaya.fragment6_8.SettingActivity;
import com.chhaya.fragment6_8.helper.FragmentHelper;

public class SettingFragment extends Fragment {

    private ListView listView;
    private String[] dataSet = {"General","Display","Sound","Battery"};

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_setting, container, false);
        listView = view.findViewById(R.id.setting_list);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                getActivity(),
                android.R.layout.simple_list_item_1,
                dataSet
        );

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (Configuration.ORIENTATION_PORTRAIT == getResources().getConfiguration().orientation) {
                    if (position == 0) {
                        try {
                            if (getActivity() != null) {
                                FragmentHelper.replace(
                                        (SettingActivity) getActivity(),
                                        R.id.f_setting_container,
                                        new GeneralSettingFragment()
                                );
                            }
                        }
                        catch (ClassCastException ex) {
                            ex.printStackTrace();
                        }
                    }
                }
            }
        });
    }
}
