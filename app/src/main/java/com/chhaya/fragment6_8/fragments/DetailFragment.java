package com.chhaya.fragment6_8.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.chhaya.fragment6_8.R;
import com.chhaya.fragment6_8.callback.DetailFragmentCallback;

public class DetailFragment extends Fragment {

    private DetailFragmentCallback callback;

    private EditText editMessage;
    private Button btnSend;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        callback = (DetailFragmentCallback) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.message_layout, container, false);
        editMessage = view.findViewById(R.id.edit_message);
        btnSend = view.findViewById(R.id.button_send);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public void setEditMessage(String message) {
        editMessage.setText(message);
    }
}
