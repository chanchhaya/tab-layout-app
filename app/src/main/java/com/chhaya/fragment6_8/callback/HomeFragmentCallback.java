package com.chhaya.fragment6_8.callback;

public interface HomeFragmentCallback {
    void send(String message);
}
