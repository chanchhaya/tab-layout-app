package com.chhaya.fragment6_8.adapters;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.chhaya.fragment6_8.fragments.TabHistoryFragment;
import com.chhaya.fragment6_8.fragments.TabHomeFragment;
import com.chhaya.fragment6_8.fragments.TabSettingFragment;

public class AppPagerAdapter extends FragmentPagerAdapter {

    private int tabCount;
    private CharSequence[] tabTitle = {"Home", "History", "Setting"};

    public AppPagerAdapter(@NonNull FragmentManager fm, int tabCount) {
        super(fm, tabCount);
        this.tabCount = tabCount;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new TabHomeFragment();break;
            case 1:
                fragment = new TabHistoryFragment();break;
            case 2:
                fragment = new TabSettingFragment();break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return tabCount;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitle[position];
    }
    
}
