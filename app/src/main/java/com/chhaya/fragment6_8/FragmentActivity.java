package com.chhaya.fragment6_8;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Toast;

import com.chhaya.fragment6_8.callback.DetailFragmentCallback;
import com.chhaya.fragment6_8.callback.HomeFragmentCallback;
import com.chhaya.fragment6_8.fragments.DetailFragment;
import com.chhaya.fragment6_8.fragments.HomeFragment;
import com.chhaya.fragment6_8.helper.FragmentHelper;

public class FragmentActivity extends AppCompatActivity implements HomeFragmentCallback, DetailFragmentCallback {

    DetailFragment detailFragment;
    HomeFragment homeFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);

        detailFragment = new DetailFragment();
        homeFragment = new HomeFragment();

        FragmentHelper.add(this, R.id.top_container, homeFragment);
        FragmentHelper.add(this, R.id.bottom_container, detailFragment);

    }

    @Override
    public void send(String message) {
        detailFragment.setEditMessage(message);
    }

    @Override
    public void get() {

    }
}
