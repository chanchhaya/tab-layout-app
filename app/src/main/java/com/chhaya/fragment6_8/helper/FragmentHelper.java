package com.chhaya.fragment6_8.helper;

import androidx.annotation.IdRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class FragmentHelper {

    static public void add(AppCompatActivity compatActivity, @IdRes int container, Fragment fragment) {
        FragmentManager fm = compatActivity.getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(container, fragment);
        ft.commit();
    }

    static public void replace(AppCompatActivity compatActivity, @IdRes int container, Fragment fragment) {
        FragmentManager fm = compatActivity.getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(container, fragment);
        ft.addToBackStack(null);
        ft.commit();
    }

}
