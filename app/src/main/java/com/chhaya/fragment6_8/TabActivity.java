package com.chhaya.fragment6_8;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.widget.Toast;

import com.chhaya.fragment6_8.adapters.AppPagerAdapter;
import com.chhaya.fragment6_8.fragments.TabHistoryFragment;
import com.chhaya.fragment6_8.fragments.TabHomeFragment;
import com.chhaya.fragment6_8.fragments.TabSettingFragment;
import com.chhaya.fragment6_8.helper.FragmentHelper;
import com.google.android.material.tabs.TabLayout;

import java.util.Objects;

public class TabActivity extends AppCompatActivity {

    private TabLayout appTab;
    private ViewPager tabBody;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab);
        initViews();
        initDefaultTabBody();
        //configureAppTab();
        configureViewPager();

        /*TabLayout.Tab selectedTab = appTab.getTabAt(2);
        if (selectedTab != null) {
            selectedTab.select();
        }*/
    }

    private void initViews() {
        appTab = findViewById(R.id.app_tab);
        tabBody = findViewById(R.id.tab_body);
    }

    private void initDefaultTabBody() {
        FragmentHelper.add(this,
                R.id.tab_body, new TabHomeFragment());
    }

    private void configureAppTab() {
        appTab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int position = tab.getPosition();
                Fragment fragment = null;
                switch (position) {
                    case 0:
                        // replace homeFragment
                        fragment = new TabHomeFragment();
                        break;
                    case 1:
                        // replace historyFragment
                        fragment = new TabHistoryFragment();
                        break;
                    case 2:
                        // replace settingFragment
                        fragment = new TabSettingFragment();
                        break;
                }
                FragmentHelper.replace(TabActivity.this,
                        R.id.tab_body, fragment);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                Toast.makeText(TabActivity.this,
                        "Unselected", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                Toast.makeText(TabActivity.this,
                        "Reselected", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void configureViewPager() {
        AppPagerAdapter appPagerAdapter = new AppPagerAdapter(
                getSupportFragmentManager(), appTab.getTabCount()
        );
        appTab.setupWithViewPager(tabBody);
        tabBody.setAdapter(appPagerAdapter);
    }

}
