package com.chhaya.fragment6_8;

import androidx.annotation.IdRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.chhaya.fragment6_8.fragments.DetailFragment;
import com.chhaya.fragment6_8.fragments.HomeFragment;
import com.chhaya.fragment6_8.helper.FragmentHelper;

public class DFragmentActivity extends AppCompatActivity {

    private Button btnReplace;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dfragment);

        btnReplace = findViewById(R.id.btn_replace);

        FragmentHelper.add(this, R.id.container, new HomeFragment());

        btnReplace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentHelper.replace(DFragmentActivity.this, R.id.container, new DetailFragment());
            }
        });

    }



}
